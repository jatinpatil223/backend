package com.example.demo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class TraderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetTraderByName() throws Exception {

		this.mockMvc.perform(get("/api/stocks/AMZN")).andDo(print()).andExpect(status().isOk()).andReturn();

	}

	@Test
	public void testStocksTickerwise() throws Exception {

		this.mockMvc.perform(get("/api/stocks/portfolio")).andDo(print()).andExpect(status().isOk()).andReturn();

	}

	@Test
	public void testGetAllTraders() throws Exception {

		this.mockMvc.perform(get("/api/stocks/")).andDo(print()).andExpect(status().isOk()).andReturn();

	}

	@Test
	public void testAddTrader() throws Exception {

		this.mockMvc.perform(get("/api/stocks/post/")).andDo(print()).andExpect(status().isOk()).andReturn();

	}

}