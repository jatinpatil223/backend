package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Trader;

@ActiveProfiles("h2")
@SpringBootTest
public class MySQLTraderRepositoryTest {

	@Autowired
	MySQLTraderRepository mySQLTraderRepository;

	@Test
	public void testGetAllShippers() {
		List<Trader> returnedList = mySQLTraderRepository.getAllTraders();
		assertThat(returnedList).isNotEmpty();
	}

	@Test
	public void testGetTraderByName() {
		List<Trader> returnedList = mySQLTraderRepository.getTraderByName("AMZN");
		assertThat(returnedList).isNotEmpty();
	}

	@Test
	public void testGetTradersTickerwise() {
		List<Portfolio> returnedList = mySQLTraderRepository.getTradersTickerwise();
		assertThat(returnedList).isNotEmpty();
	}

	@Test
	public void testAddTrader() {
		Trader returnedList = mySQLTraderRepository.addTrader(new Trader(1, "Buy", "GAIL", (float) 56.2, 200));
		assertThat(returnedList).isNotNull();
	}
}
