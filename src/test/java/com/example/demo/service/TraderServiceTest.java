package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Trader;
import com.example.demo.repository.TraderRepository;

@ActiveProfiles("h2")
@SpringBootTest
public class TraderServiceTest {

	@Autowired
	TraderService traderService;

	@MockBean
	TraderRepository traderRepository;

	@Test
	public void testGetTraderByName() {
		String testName = "AMZN";
		Trader testTrader = new Trader();
		testTrader.setTicker(testName);
		List<Trader> testTraderList = null;

		when(traderRepository.getTraderByName(testTrader.getTicker())).thenReturn(testTraderList);

		List<Trader> returnedTraderList = traderService.getTraderByName(testName);

		assertThat(returnedTraderList).isEqualTo(testTraderList);
	}

	@Test
	public void testGetTraders() {
		List<Trader> testTraderList = null;

		when(traderRepository.getAllTraders()).thenReturn(testTraderList);

		List<Trader> returnedTraderList = traderService.getAllTraders();

		assertThat(returnedTraderList).isEqualTo(testTraderList);
	}

	@Test
	public void testGetStocksTickerwise() {
		List<Portfolio> testTraderList = null;

		when(traderRepository.getTradersTickerwise()).thenReturn(testTraderList);

		List<Portfolio> returnedTraderList = traderService.getStocksTickerwise();

		assertThat(returnedTraderList).isEqualTo(testTraderList);
	}

}
