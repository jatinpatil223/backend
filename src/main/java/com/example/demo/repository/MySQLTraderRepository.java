package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Trader;

@Repository
public class MySQLTraderRepository implements TraderRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Trader> getAllTraders() {
		// TODO Auto-generated method stub
		String sql = "SELECT ID, buyOrSell, Ticker, abs(Price) as Price, abs(Quantity) as Quantity FROM stock";
		return template.query(sql, new TraderRowMapper());

	}

	@Override
	public List<Portfolio> getTradersTickerwise() {
		String sql = "SELECT Ticker, sum(Quantity) as Quantity, sum((Price)*(-Quantity)) as NetAmount FROM stock group by Ticker";
		return template.query(sql, new SumRowMapper());

	}

	@Override
	public List<Trader> getTraderByName(String name) {
		// TODO Auto-generated method stub
		String sql = "SELECT id, ticker, buyOrSell, Price, Quantity FROM stock WHERE ticker=?";
		return template.query(sql, new TraderRowMapper(), name);
	}

	/*
	 * @Override public Trader editTrader(Trader trader) { // TODO Auto-generated
	 * method stub String sql = "UPDATE Shippers SET ticker = ?, Price = ? " +
	 * "WHERE id = ?" + "buyOrSell = ?"; template.update(sql, trader.getTicker(),
	 * trader.getPrice(), trader.getId(), trader.getBuyOrSell()); return trader; }
	 * 
	 * @Override public int deleteTrader(int id) { // TODO Auto-generated method
	 * stub String sql = "DELETE FROM Trade WHERE id = ?"; template.update(sql, id);
	 * return id; }
	 */
	@Override
	public Trader addTrader(Trader trader) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO stock (BuyOrSell,Ticker, Price, Quantity) " + "VALUES(?,?,?,?)";
		template.update(sql, trader.getBuyOrSell(), trader.getTicker(), trader.getPrice(), trader.getQuantity());
		return trader;
	}
}

class TraderRowMapper implements RowMapper<Trader> {

	@Override
	public Trader mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Trader(rs.getInt("ID"), rs.getString("BuyOrSell"), rs.getString("Ticker"), rs.getFloat("Price"),
				rs.getInt("Quantity"));
	}

}

class SumRowMapper implements RowMapper<Portfolio> {

	@Override
	public Portfolio mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new Portfolio(rs.getString("Ticker"), rs.getInt("Quantity"), rs.getFloat("NetAmount"));
	}

}
