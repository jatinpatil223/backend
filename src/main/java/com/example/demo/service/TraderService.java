package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Trader;
import com.example.demo.repository.TraderRepository;

@Service
public class TraderService {
	@Autowired
	private TraderRepository repository;

	public List<Trader> getAllTraders() {
		return repository.getAllTraders();
	}

	public List<Trader> getTraderByName(String name) {
		return repository.getTraderByName(name);
	}

	/*
	 * public Trader saveTrader(Trader trader) { return
	 * repository.editTrader(trader); }
	 */
	public Trader newTrader(Trader trader) {
		if (trader.getBuyOrSell().equals("Sell")) {
			trader.setQuantity((-1) * trader.getQuantity());
		}
		return repository.addTrader(trader);
	}

	public List<Portfolio> getStocksTickerwise() {
		return repository.getTradersTickerwise();
	}
	/*
	 * public int deleteTrader(int id) { return repository.deleteTrader(id); }
	 */
}
