package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackathonApplication.class, args);
	}

}

/*
 * linux mysql login: hostname: ...devops...conygre.com user: conygre password:
 * C0nygre-C0nygre
 */

/*
 * spring.datasource.url=jdbc:mysql://punedevopsb3.conygre.com:3306/stockdb
 * spring.datasource.username=conygre spring.datasource.password=C0nygre-C0nygre
 * 
 */