package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Portfolio;
import com.example.demo.entities.Trader;
import com.example.demo.service.TraderService;

@CrossOrigin("*")
@RestController
@RequestMapping("api/stocks")
public class TraderController {

	@Autowired
	TraderService service;

	@GetMapping(value = "/")
	public List<Trader> getAllTraders() {
		return service.getAllTraders();
	}

	@GetMapping(value = "/portfolio")
	public List<Portfolio> getStocksTickerwise() {
		return service.getStocksTickerwise();
	}

	@GetMapping(value = "/{name}")
	public List<Trader> getTraderByName(@PathVariable("name") String name) {
		return service.getTraderByName(name);
	}

	@PostMapping(value = "/post")
	public Trader addTrader(@RequestBody Trader trader) {
		return service.newTrader(trader);
	}
	/*
	 * @PutMapping(value = "/") public Trader editTrader(@RequestBody Trader trader)
	 * { return service.saveTrader(trader); }
	 * 
	 * @DeleteMapping(value = "/{id}") public int deleteTrader(@PathVariable int id)
	 * { return service.deleteTrader(id); }
	 */
}
