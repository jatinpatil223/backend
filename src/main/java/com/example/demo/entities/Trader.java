
package com.example.demo.entities;

public class Trader {
	private int id;
	private String ticker;
	private float price;
	private String buyOrSell;
	private int quantity;

	public Trader() {

	}

	public Trader(int id, String buyOrSell, String ticker, float price, int quantity) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.ticker = ticker;
		this.price = price;
		this.buyOrSell = buyOrSell;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
