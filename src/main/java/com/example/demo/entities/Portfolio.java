package com.example.demo.entities;

public class Portfolio {
	private String ticker;
	private int quantity;
	private float price;

	public Portfolio() {

	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Portfolio(String ticker, int quantity, float price) {
		super();
		this.ticker = ticker;
		this.quantity = quantity;
		this.price = price;
	}

}
